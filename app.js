var express = require('express');
var mongodb = require('mongodb');
var app = express();

mongodb.MongoClient.connect(process.env.MONGODB_URI, function(err, db) {
  if(err) {
    console.error('Failed to make all database connections!');
    console.error(err);
    process.exit(1);
  }

  console.log('Connected successfully to database!')

  var test = db.collection('test');

  // Create get method
  app.get('/', function(req, res) {
    test.insertOne({});

    res.send('Hello World!');
  });

  app.listen(process.env.PORT, function() {
    console.log('Listening on port %s', process.env.PORT);
  });
});
